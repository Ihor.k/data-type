<?php
$boolTrue = true;
$boolFalse = false;
$action = "123";
$string = "string";
if ($action == "123") {
    echo "$string. == Перевірка пройшла вдало";
    echo '<br>';
}
if ($action === "123"){
    echo '$string. === Перевірка пройшла вдало';
    echo '<br>';
} else {
    echo '$string. === Перевірка не пройшла';
    echo '<br>';
}
//integer
var_dump(intdiv(10,2));
var_dump(round(1231.312423424,3));
var_dump(intval("123.123"));
var_dump((int)"123.123");
var_dump((int)"50.50");
//string
var_dump($string);
$a = "test";
var_dump($a[3]);
var_dump($a[2]);
var_dump($a[1]);
var_dump($a[0]);
$String = 'apple';
$String ="$String and orange";
$String ="{$String}s";
var_dump($String);
?>


